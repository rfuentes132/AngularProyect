'use strict';

angular
  .module('angularProyect')
  .factory('Twit', function (API, $resource) {
    return $resource(API.tweets,
    {id: '@id'},
    {
      update: {
        method: 'put'
      }
    });
  });