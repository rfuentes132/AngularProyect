'use strict';

angular
  .module('angularProyect')
  .factory('Session', function ($http, $window) {

    return {
      access: function (email, password, API, callback) {
        
        $http({
          method: 'post',
          url: API.access,
          data: {
            email: email,
            password: password
          }
        })
        .success(function (userResponse) {
          if (callback) callback(userResponse);
          $window.sessionStorage.user = userResponse.token;        
        }); 
      }
    }

  })