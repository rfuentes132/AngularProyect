  'use strict';

angular
  .module('angularProyect')
  .factory('User', function (API, $resource) {
    return $resource(API.users,
    {id: '@id'},
    {
      update: {
        method: 'put'
      },
      follow: {
        method: 'post',
        url: API.follows
      },
      unfollow:{
        method: 'delete',
        url: API.follows
      }
    });
  });