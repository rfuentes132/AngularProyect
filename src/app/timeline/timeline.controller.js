'use strict';

angular
	.module('angularProyect')
	.controller('TimelineController', function ($window, $state, Twit, User) {

		var tl = this;
		tl.twit = {};
		tl.twits = [];
		tl.twits = Twit.query();
		tl.users = User.query();

		tl.send = function () {
			console.log(tl.twit);
			tl.twits.push(Twit.save(tl.twit));
			tl.twit = {};
		}
		
		tl.getTwits = function () {
			tl.twits = Twit.query();
		};

		tl.follow = function (index) {
			// console.log("trying");
			// console.log(tl.users[index]);
			tl.users[index].$follow(); 
		};

		tl.unfollow = function (index) {
			tl.users[index].$unfollow(); 
		};

		tl.access = function () {
      return !!true; 
    }

	})

