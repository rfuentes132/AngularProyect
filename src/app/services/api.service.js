'use strict';

angular
  .module('angularProyect')
  .service('API', function (baseUrl) {

    var toUrl = function (path) {
      return baseUrl+path;
    }

    this.users = toUrl('/twitters/:id');
    this.tweets = toUrl('/tweets/:id');
    this.follows = toUrl('/twitters/:id/follows');
    this.access = toUrl('/access');
    this.register = toUrl('/twitters');
  });