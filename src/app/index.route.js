'use strict';

  angular
    .module('angularProyect')
    .config(function routerConfig($stateProvider, $urlRouterProvider) {
    
    $stateProvider
      .state('anon', {
        abstract: true,
        template: '<div ui-view></div>',
         resolve: {
          logged: function ($q, $injector) {
            if ($injector.get('Token').has()) {
              console.log("trying");
              return $q.reject("NotAnon");
            }
          }
        }
      })
      .state('anon.register', {
        url: '/register',
        templateUrl: 'app/register/register.html',
        controller: 'RegisterController',
        controllerAs: 'register'
      })
      .state('anon.login', {
          url: '/login',
          templateUrl: 'app/login/login.html',
          controller: 'LoginController',
          controllerAs: 'login'
      });

   
    $stateProvider
      .state('user', {
        abstract: true,
        template: '<div ui-view></div>',
        resolve: {
          security: function($q, $injector){
            if (!$injector.get('Token').has()) {
              return $q.reject('NotAuthorized');
            }
          }
        }
      })
      .state('user.timeline', {
        url: '/timeline',
        templateUrl: 'app/timeline/timeline.html',
        controller: 'TimelineController',
        controllerAs: 'timeline'
      });

      $urlRouterProvider.otherwise('/register');
    
});
