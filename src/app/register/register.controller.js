(function() {
  'use strict';
  angular
    .module('angularProyect')
    .controller('RegisterController', function ($http, API) {
    var register = this

    register.user = {
      user_attributes:{}
    }

    register.access = function () {
      console.log('registrando usuario');
      $http({
        method: 'POST',
        url: API.register,
        data:register.user
      }).success(function (data){
        console.log(data);
      })
    }
  });

})();
