'use strict';

angular
  .module('angularProyect')
  .controller('LoginController', function ($state, $window, $http, Session, API) {
    
    var login = this;

    login.access = function () {
      Session.access(login.email, login.password, API, function () {
        $state.go('user.timeline');            
      });    
    };
    
  });