(function() {
  'use strict';

  angular
    .module('angularProyect')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
